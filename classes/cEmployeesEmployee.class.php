<?php
/**
 * Employees - Employee
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

/**
 * Employees Employee class
 */
class cEmployeesEmployee extends cObject{

  /** Parameters */
  static protected $table="employees__employees";
  static protected $logs=true;

  /** Properties */
  protected $id;
  protected $deleted;
  protected $fiscal;
  protected $firstname;
  protected $lastname;
  protected $birthday;
  protected $area;
  protected $costCenter;

  /**
   * Get Label
   */
  public function getLabel($id=false){
   $label=ucwords(strtolower($this->lastname." ".$this->firstname));
   if($id){$label.=" (".$this->id.")";}
   return $label;
  }

  /**
   * Check
   *
   * @return boolean
   * @throws Exception
   */
  protected function check(){
   // check properties
   if(!strlen(trim($this->fiscal))){throw new Exception("Employee fiscal is mandatory..");}
   if(!strlen(trim($this->firstname))){throw new Exception("Employee firstname is mandatory..");}
   if(!strlen(trim($this->lastname))){throw new Exception("Employee lastname is mandatory..");}
   // return
   return true;
  }

  /**
   * Edit form
   *
   * @param string[] $additional_parameters Array of url additional parameters
   * @return object Form structure
   */
  public function form_edit(array $additional_parameters=null){
   // build form
   $form=new strForm(api_url(array_merge(["mod"=>"employees","scr"=>"submit","act"=>"employee_store","idEmployee"=>$this->id],$additional_parameters)),"POST",null,null,"employees_employees_edit_form");
   // check existence
   $form->addField("text","fiscal",api_text("cEmployeesEmployee-property-fiscal"),$this->fiscal,api_text("cEmployeesEmployee-placeholder-fiscal"),null,null,null,"required");
   $form->addField("text","firstname",api_text("cEmployeesEmployee-property-firstname"),$this->firstname,api_text("cEmployeesEmployee-placeholder-firstname"),null,null,null,"required");
   $form->addField("text","lastname",api_text("cEmployeesEmployee-property-lastname"),$this->lastname,api_text("cEmployeesEmployee-placeholder-lastname"),null,null,null,"required");
   $form->addField("date","birthday",api_text("cEmployeesEmployee-property-birthday"),$this->birthday);
   $form->addField("text","area",api_text("cEmployeesEmployee-property-area"),$this->area,api_text("cEmployeesEmployee-placeholder-area"));
   $form->addField("text","costCenter",api_text("cEmployeesEmployee-property-costCenter"),$this->costCenter,api_text("cEmployeesEmployee-placeholder-costCenter"));
   // controls
   $form->addControl("submit",api_text("form-fc-submit"));
   // return
   return $form;
  }

  // Disable remove function
  public function remove(){throw new Exception("Employee remove function disabled by developer..");}

  // debug
  //protected function event_triggered($event){api_dump($event,static::class." event triggered");}

 }

?>