<?php
/**
 * Employees - Submit
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */

 // debug
 api_dump($_REQUEST,"_REQUEST");
 // switch action
 switch(ACTION){
  // employees
  case "employee_store":employee("store");break;
  case "employee_delete":employee("delete");break;
  case "employee_undelete":employee("undelete");break;
  case "employee_remove":employee("remove");break;
  // default
  default:
   api_alerts_add(api_text("alert_submitFunctionNotFound",[MODULE,SCRIPT,ACTION]),"danger");
   api_redirect("?mod=".MODULE);
 }

 /**
  * Employee
  *
  * @param string $action Object action
  */
 function employee($action){
  // check authorizations
  api_checkAuthorization("employees-manage","dashboard");
  // get object
  $employee_obj=new cEmployeesEmployee($_REQUEST['idEmployee']);
  api_dump($employee_obj,"employee object");
  // check object
  if($action!="store" && !$employee_obj->id){api_alerts_add(api_text("cEmployeesEmployee-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=employees_list");}
  // execution
  try{
   switch($action){
    case "store":
     $employee_obj->store($_REQUEST);
     api_alerts_add(api_text("cEmployeesEmployee-alert-stored"),"success");
     break;
    case "delete":
     $employee_obj->delete();
     api_alerts_add(api_text("cEmployeesEmployee-alert-deleted"),"warning");
     break;
    case "undelete":
     $employee_obj->undelete();
     api_alerts_add(api_text("cEmployeesEmployee-alert-undeleted"),"warning");
     break;
    case "remove":
     $employee_obj->remove();
     api_alerts_add(api_text("cEmployeesEmployee-alert-removed"),"warning");
     break;
    default:
     throw new Exception("Employee action \"".$action."\" was not defined..");
   }
   // redirect
   api_redirect(api_return_url(["scr"=>"employees_list","idEmployee"=>$employee_obj->id]));
  }catch(Exception $e){
   // dump, alert and redirect
   api_redirect_exception($e,api_url(["scr"=>"employees_list","idEmployee"=>$employee_obj->id]),"cEmployeesEmployee-alert-error");
  }
 }

?>