<?php
/**
 * Employees - Template
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // build application
 $app=new strApplication();
 // build nav object
 $nav=new strNav("nav-tabs");
 // dashboard
 $nav->addItem(api_icon("fa-th-large",null,"hidden-link"),api_url(["scr"=>"dashboard"]));
 // employees
 if(explode("_",SCRIPT)[0]=="employees"){
  $nav->addItem(api_text("nav-employees-list"),api_url(["scr"=>"employees_list"]));
  // operations
  if($employee_obj->id && in_array(SCRIPT,array("employees_view","employees_edit"))){
   $nav->addItem(api_text("nav-operations"),null,null,"active");
   $nav->addSubItem(api_text("nav-employees-operations-edit"),api_url(["scr"=>"employees_edit","idEmployee"=>$employee_obj->id]),(api_checkAuthorization("employees-manage")));
  }else{
   $nav->addItem(api_text("nav-employees-add"),api_url(["scr"=>"employees_edit"]),(api_checkAuthorization("employees-manage")));
  }
 }
 // add nav to html
 $app->addContent($nav->render(false));
?>