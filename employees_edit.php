<?php
/**
 * Employees - Employees Edit
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("employees-manage","dashboard");
 // get objects
 $employee_obj=new cEmployeesEmployee($_REQUEST['idEmployee']);
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(($employee_obj->id?api_text("employees_edit",$employee_obj->getLabel()):api_text("employees_edit-add")));
 // get form
 $form=$employee_obj->form_edit(["return"=>api_return(["scr"=>"employees_view"])]);
 // additional controls
 if($employee_obj->id){
  $form->addControl("button",api_text("form-fc-cancel"),api_return_url(["scr"=>"employees_view","idEmployee"=>$employee_obj->id]));
  if(!$employee_obj->deleted){
   $form->addControl("button",api_text("form-fc-delete"),api_url(["scr"=>"submit","act"=>"employee_delete","idEmployee"=>$employee_obj->id]),"btn-danger",api_text("employees_edit-fc-delete-confirm"));
  }else{
   $form->addControl("button",api_text("form-fc-undelete"),api_url(["scr"=>"submit","act"=>"employee_undelete","idEmployee"=>$employee_obj->id,"return"=>["scr"=>"employees_view"]]),"btn-warning");
   $form->addControl("button",api_text("form-fc-remove"),api_url(["scr"=>"submit","act"=>"employee_remove","idEmployee"=>$employee_obj->id]),"btn-danger",api_text("employees_edit-fc-remove-confirm"));
  }
 }else{$form->addControl("button",api_text("form-fc-cancel"),api_url(["scr"=>"employees_list"]));}
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($form->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($employee_obj,"employee");
?>