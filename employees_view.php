<?php
/**
 * Employees - Employees View
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 api_checkAuthorization("employees-usage","dashboard");
 // get objects
 $employee_obj=new cEmployeesEmployee($_REQUEST['idEmployee']);
 // check objects
 if(!$employee_obj->id){api_alerts_add(api_text("cEmployeesEmployee-alert-exists"),"danger");api_redirect("?mod=".MODULE."&scr=employees_list");}
 // deleted alert
 if($employee_obj->deleted){api_alerts_add(api_text("cEmployeesEmployee-warning-deleted"),"warning");}
 // include module template
 require_once(MODULE_PATH."template.inc.php");
 // set application title
 $app->setTitle(api_text("employees_view",$employee_obj->getLabel()));
 // check for tab
 if(!defined(TAB)){define("TAB","informations");}
 // build employee description list
 $dl=new strDescriptionList("br","dl-horizontal");
 $dl->addElement(api_text("employees_view-dt-id"),api_tag("samp",$employee_obj->id));
 $dl->addElement(api_text("cEmployeesEmployee-property-label"),api_tag("strong",$employee_obj->getLabel()));
 if($employee_obj->description){$right_dl->addElement(api_text("cEmployeesEmployee-property-description"),nl2br($employee_obj->description));}
 // build informations description list
 $informations_dl=new strDescriptionList("br","dl-horizontal");
 $informations_dl->addElement(api_text("cEmployeesEmployee-property-fiscal"),api_tag("samp",$employee_obj->fiscal));
 $informations_dl->addElement(api_text("cEmployeesEmployee-property-birthday"),api_date_format($employee_obj->birthday,api_text("date")));
 $informations_dl->addElement(api_text("cEmployeesEmployee-property-area"),$employee_obj->area);
 $informations_dl->addElement(api_text("cEmployeesEmployee-property-costCenter"),$employee_obj->costCenter);
 // build view tabs
 $tab=new strTab();
 $tab->addItem(api_icon("fa-flag-o")." ".api_text("employees_view-tab-informations"),$informations_dl->render(),("informations"==TAB?"active":null));
 $tab->addItem(api_icon("fa-file-text-o")." ".api_text("employees_view-tab-logs"),api_logs_table($employee_obj->getLogs())->render(),("logs"==TAB?"active":null));
 // check for status actions
 if(ACTION=="status" && api_checkAuthorization("employees-manage")){
  // get form
  $form=$employee_obj->form_status(["return"=>["scr"=>"employees_view"]]);
  // additional controls
  $form->addControl("button",api_text("form-fc-cancel"),"#",null,null,null,"data-dismiss='modal'");
  // build modal
  $modal=new strModal(api_text("employees_view-status-modal-title"),null,"employees_view-status");
  $modal->setBody($form->render(2));
  // add modal to application
  $app->addModal($modal);
  // modal scripts
  $app->addScript("$(function(){\$('#modal_employees_view-status').modal({show:true,backdrop:'static',keyboard:false});});");
 }
 // build grid object
 $grid=new strGrid();
 $grid->addRow();
 $grid->addCol($dl->render(),"col-xs-12");
 $grid->addRow();
 $grid->addCol($tab->render(),"col-xs-12");
 // add content to application
 $app->addContent($grid->render());
 // renderize application
 $app->render();
 // debug
 api_dump($employee_obj,"employee");
?>