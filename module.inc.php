<?php
/**
 * Employees
 *
 * @package Coordinator\Modules\Employees
 * @company Cogne Acciai Speciali s.p.a
 * @authors Manuel Zavatta <manuel.zavatta@cogne.com>
 */
 // definitions
 $module_name="employees";
 $module_repository_url="https://bitbucket.org/SISCAS/cf-employees/";
 $module_repository_version_url="https://bitbucket.org/SISCAS/cf-employees/raw/master/VERSION.txt";
 $module_required_modules=array();
?>