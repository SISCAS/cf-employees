--
-- Employees - Update (1.0.1)
--
-- @package Coordinator\Modules\Employees
-- @company Cogne Acciai Speciali s.p.a
-- @authors Manuel Zavatta <manuel.zavatta@cogne.com>
--

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- --------------------------------------------------------

-- Add properties to employees
ALTER TABLE `employees__employees` ADD `area` VARCHAR(32) NULL AFTER `birthday`;
ALTER TABLE `employees__employees` ADD `costCenter` VARCHAR(10) NULL AFTER `area`;

-- --------------------------------------------------------

SET FOREIGN_KEY_CHECKS = 1;

-- --------------------------------------------------------
